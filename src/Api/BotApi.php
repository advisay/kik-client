<?php

namespace Advisay\Kik\Api;

use Exception;
use Advisay\Kik\Api\Types\Configuration;
use Advisay\Kik\Api\Types\KikCode;
use Advisay\Kik\Api\Types\UserProfile;

/**
 * Class BotApi
 *
 * @package Advisay\Kik\Api
 */
class BotApi
{
    /**
     * Request type GET
     */
    const TYPE_GET = "get";

    /**
     * Request type POST
     */
    const TYPE_POST = "post";

    /**
     * API Url prefix
     *
     * @var string
     */
    const URL_PREFIX = 'https://api.kik.com/v1/';

    /**
     * Bot username
     *
     * @var string|null
     */
    protected $username = null;

    /**
     * Bot API Key
     *
     * @var string|null
     */
    protected $apiKey = null;

    /**
     * CURL object
     *
     * @var
     */
    protected $curl;


    /**
     * BotApi constructor.
     *
     * @param string $username Kik Bot username
     * @param string $api_key Kik Bot API key
     */
    public function __construct($username, $api_key)
    {
        $this->username = $username;
        $this->apiKey = $api_key;

        $this->curl = curl_init();
        curl_setopt($this->curl, CURLOPT_USERPWD, $this->username.":".$this->apiKey);
        curl_setopt($this->curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
    }

    /**
     * @param string|null $action
     * @return string
     */
    public function getUrl($action = null)
    {
        return $action ? self::URL_PREFIX.$action : self::URL_PREFIX;
    }

    /**
     * Close curl
     */
    public function __destruct()
    {
        $this->curl && curl_close($this->curl);
    }

    /**
     * Call method.
     *
     * @param string $action
     * @param array|string|null $data
     * @param string $type
     *
     * @return mixed
     *
     * @throws Exception
     */
    public function call($action, $data = null, $type = self::TYPE_POST)
    {
        curl_setopt($this->curl, CURLOPT_URL, $this->getUrl($action));
        curl_setopt($this->curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);

        if ($type == self::TYPE_POST) {
            curl_setopt($this->curl, CURLOPT_POST, 1);
            curl_setopt($this->curl, CURLOPT_POSTFIELDS, json_encode($data));
        }

        $result = curl_exec($this->curl);
        if ($result === false) {
            throw new Exception(curl_error($this->curl), curl_errno($this->curl));
        }

        return json_decode($result, true);
    }

    /**
     * Get bot current configuration.
     *
     * @return Configuration|BaseType|null
     * @throws Exception
     */
    public function getConfiguration()
    {
        $response = $this->call('config', [], self::TYPE_GET);

        return Configuration::fromResponse($response);
    }

    /**
     * Set bot new configuration.
     *
     * @param Configuration $configuration
     * @return Configuration|BaseType|null
     * @throws Exception
     */
    public function setConfiguration($configuration)
    {
        $response = $this->call('config', $configuration->toJson(true), self::TYPE_POST);

        return Configuration::fromResponse($response);
    }

    /**
     * Get a User's Profile who have subscribed to a bot.
     *
     * @param string $username
     * @return UserProfile|BaseType|null
     * @throws Exception
     */
    public function getUserProfile($username)
    {
        $response = $this->call('user/'.$username, [], self::TYPE_GET);

        return UserProfile::fromResponse($response);
    }

    /**
     * Get a Kik Code url.
     *
     * @param string $code_id Hex-encoded 160-bit remote ID for the Kik Code
     * @param int|null $color_code The integer color code indicating the color that should be shown when the user scans.
     * @return string
     */
    public static function getKikCodeUrl($code_id, $color_code = null)
    {
        return self::URL_PREFIX.'code/'.$code_id.(isset($color_code) && $color_code < 16 ? '?c='.$color_code : '');
    }

    /**
     * Create a new Kik Code.
     *
     * @param string $payload
     * @return KikCode|BaseType|null
     * @throws Exception
     */
    public function createKikCode($payload)
    {
        if (!$payload) {
            return null;
        }

        $response = $this->call('code', ['data' => $payload], self::TYPE_POST);

        return KikCode::fromResponse($response);
    }

    /**
     * Send messages.
     *
     * Note: Bots are allowed to send up to 5 messages per user per batch. If you attempt to send more than 5 messages
     * to single user in any batch, all messages in that batch will not be sent. If you attempt to send messages to users
     * that are not subscribed to your bot, or have blocked your bot, the entire offending batch will not be sent.
     *
     * @param array $messages
     * @return mixed
     * @throws Exception
     */
    public function sendMessage(array $messages)
    {
        $data = [];
        foreach ($messages as $message) {
            $data[] = $message->toJson(true);
        }
        $response = $this->call('message', ['messages' => $data], self::TYPE_POST);

        return $response;
    }
}
