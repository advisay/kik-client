<?php

namespace Advisay\Kik\Api;

interface TypeInterface
{
    public static function fromResponse($data);
}
