<?php

namespace Advisay\Kik\Api\Types\Keyboard;

/**
 * Class ArrayOfKeyboards
 *
 * @package Advisay\Kik\Api\Types\Keyboard
 */
abstract class ArrayOfKeyboards
{
    /**
     * Read array of Keyboards from response.
     *
     * @param array $data
     * @return array
     * @throws \Exception
     */
    public static function fromResponse($data)
    {
        $arrayOfKeyboards = [];
        foreach ($data as $keyboardItem) {
            $arrayOfKeyboards[] = Keyboard::fromResponse($keyboardItem);
        }

        return $arrayOfKeyboards;
    }
}
