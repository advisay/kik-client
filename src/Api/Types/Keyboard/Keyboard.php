<?php

namespace Advisay\Kik\Api\Types\Keyboard;

use Advisay\Kik\Api\BaseType;
use Advisay\Kik\Api\TypeInterface;
use Advisay\Kik\Api\Types\Keyboard\Response\ArrayOfKeyboardResponses;

/**
 * Class Keyboard
 * Keyboard objects describe the information that is delivered to Kik clients that allow them to render different keyboards to respond to your bot.
 *
 * @package Advisay\Kik\Api\Types\Keyboard
 */
class Keyboard extends BaseType implements TypeInterface
{
    /**
     * {@inheritdoc}
     *
     * @var array
     */
    static protected $requiredParams = ['type', 'responses'];

    /**
     * {@inheritdoc}
     *
     * @var array
     */
    static protected $map = [
        'to' => true,
        'hidden' => true,
        'type' => true,
        'responses' => ArrayOfKeyboardResponses::class,
    ];

    /**
     * The type of keyboard to send.
     * Currently, this value must be "suggested", as this is the only custom keyboard available to send to users.
     *
     * @var string
     */
    protected $type = 'suggested';

    /**
     * A list of suggested response objects that will be shown for the users that receive this keyboard.
     * Each user can be sent up to 20 suggested responses.
     *
     * @var array
     */
    protected $responses;

    /**
     * The username of the user that will receive this keyboard.
     * Can be omitted to send this keyboard to all users that haven't been otherwise named in other keyboard objects.
     *
     * @var string
     */
    protected $to;

    /**
     * If true, hides the custom keyboard behind the user's default keyboard.
     * By default, this keyboard will be shown to the user first.
     *
     * @var boolean
     */
    protected $hidden = false;

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return array
     */
    public function getResponses()
    {
        return $this->responses;
    }

    /**
     * @param array $responses
     */
    public function setResponses(array $responses)
    {
        $this->responses = $responses;
    }

    /**
     * @return string
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param string $to
     */
    public function setTo($to)
    {
        $this->to = $to;
    }

    /**
     * @return bool
     */
    public function isHidden()
    {
        return $this->hidden;
    }

    /**
     * @param boolean $hidden
     */
    public function setHidden($hidden)
    {
        $this->hidden = (bool)$hidden;
    }
}
