<?php

namespace Advisay\Kik\Api\Types\Keyboard\Response;

use Advisay\Kik\Api\Types\Keyboard\Response\KeyboardResponse;

/**
 * Class PictureKeyboardResponse
 * A picture suggested response is designed to guide the chat flow for the user in a meaningful and entertaining way.
 *
 * @package Advisay\Kik\Api\Types\Keyboard\Response
 */
class PictureKeyboardResponse extends KeyboardResponse
{
    /**
     * {@inheritdoc}
     *
     * @var array
     */
    static protected $requiredParams = ['type', 'picUrl', 'metadata'];

    /**
     * {@inheritdoc}
     *
     * @var array
     */
    static protected $map = [
        'type' => true,
        'metadata' => true,
        'picUrl' => true,
    ];


    /**
     * PictureKeyboardResponse constructor.
     * @param string|null $picUrl
     * @param mixed $metadata
     */
    public function __construct($picUrl = null, $metadata = null)
    {
        $this->type = 'picture';
        $this->picUrl = $picUrl;
        $this->metadata = $metadata;
    }
}
