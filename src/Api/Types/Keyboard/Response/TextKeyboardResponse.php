<?php

namespace Advisay\Kik\Api\Types\Keyboard\Response;

use Advisay\Kik\Api\Types\Keyboard\Response\KeyboardResponse;

/**
 * Class TextKeyboardResponse
 * A Text response keyboard presents a set of predefined options for the user.
 *
 * @package Advisay\Kik\Api\Types\Keyboard\Response
 */
class TextKeyboardResponse extends KeyboardResponse
{
    /**
     * {@inheritdoc}
     *
     * @var array
     */
    static protected $requiredParams = ['type', 'body'];

    /**
     * {@inheritdoc}
     *
     * @var array
     */
    static protected $map = [
        'type' => true,
        'metadata' => true,
        'body' => true,
    ];

    /**
     * TextKeyboardResponse constructor.
     *
     * @param string|null $body
     * @param mixed $metadata
     */
    public function __construct($body = null, $metadata = null)
    {
        $this->type = 'text';
        $this->body = $body;
        $this->metadata = $metadata;
    }
}
