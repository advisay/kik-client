<?php

namespace Advisay\Kik\Api\Types\Keyboard\Response;

/**
 * Class ArrayOfKeyboardResponses
 *
 * @package Advisay\Kik\Api\Types\Keyboard\Response
 */
abstract class ArrayOfKeyboardResponses
{
    /**
     * Read array of KeyboardResponse from response.
     *
     * @param array $data
     * @return array
     * @throws \Exception
     */
    public static function fromResponse($data)
    {
        $arrayOfResponses = [];
        foreach ($data as $responsesItem) {
            $arrayOfResponses[] = KeyboardResponse::fromResponse($responsesItem);
        }

        return $arrayOfResponses;
    }
}
