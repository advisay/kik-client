<?php

namespace Advisay\Kik\Api\Types\Keyboard\Response;

use Advisay\Kik\Api\BaseType;
use Advisay\Kik\Api\TypeInterface;

/**
 * Class KeyboardResponse
 * A suggested response keyboard presents a set of predefined options for the user.
 *
 * @package Advisay\Kik\Api\Types\Keyboard\Response
 */
class KeyboardResponse extends BaseType implements TypeInterface
{
    /**
     * {@inheritdoc}
     *
     * @var array
     */
    static protected $requiredParams = ['type'];

    /**
     * {@inheritdoc}
     *
     * @var array
     */
    static protected $map = [
        'type' => true,
        'metadata' => true,
        'body' => true,
        'picUrl' => true,
        'min' => true,
        'max' => true,
        'preselected' => true,
    ];

    /**
     * The type of response to send. Must be one of: text, friend-picker, picture
     *
     * @var string
     */
    protected $type;

    /**
     * If you require some context when users selects a suggested response, you can include an object to be returned
     * back to your bot when the user responds using the suggested response. This may be a string or object, as needed.
     *
     * @var string
     */
    protected $metadata;

    /**
     * The text to be shown to the user on the suggested response. Maximum body length of 100 characters.
     *
     * @var string
     */
    protected $body;

    /**
     * The image that will be shown to the user.
     *
     * @var string
     */
    protected $picUrl;

    /**
     * The minimum amount of friends the user can invite, must be between 1 - 100 and less than or equal to max.
     *
     * @var int
     */
    protected $min;

    /**
     * The maximum amount of friends the user can invite, must be between 1 - 100 and greater than or equal to min.
     *
     * @var int
     */
    protected $max;

    /**
     * A predetermined list of users to be picked by the friend picker.
     *
     * @var array
     */
    protected $preselected;


    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * @param string $metadata
     */
    public function setMetadata($metadata)
    {
        $this->metadata = $metadata;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param string $body
     */
    public function setBody($body)
    {
        $this->body = $body;
    }

    /**
     * @return string
     */
    public function getPicUrl()
    {
        return $this->picUrl;
    }

    /**
     * @param string $picUrl
     */
    public function setPicUrl($picUrl)
    {
        $this->picUrl = $picUrl;
    }

    /**
     * @return int
     */
    public function getMin()
    {
        return $this->min;
    }

    /**
     * @param int $min
     */
    public function setMin($min)
    {
        $this->min = $min;
    }

    /**
     * @return int
     */
    public function getMax()
    {
        return $this->max;
    }

    /**
     * @param int $max
     */
    public function setMax($max)
    {
        $this->max = $max;
    }

    /**
     * @return array
     */
    public function getPreselected()
    {
        return $this->preselected;
    }

    /**
     * @param array $preselected
     */
    public function setPreselected(array $preselected)
    {
        $this->preselected = $preselected;
    }
}
