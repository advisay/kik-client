<?php

namespace Advisay\Kik\Api\Types\Message\Received;

use Advisay\Kik\Api\Types\Message\Attribution;

/**
 * Class LinkMessage
 * These apply to all Link messages that are sent to your bot.
 *
 * @package Advisay\Kik\Api\Types\Message\Received
 */
class LinkMessage extends Message
{
    /**
     * {@inheritdoc}
     *
     * @var array
     */
    static protected $requiredParams = [
        'type',
        'id',
        'chatId',
        'mention',
        'from',
        'readReceiptRequested',
        'timestamp',
        'participants',
        'url',
    ];

    /**
     * {@inheritdoc}
     *
     * @var array
     */
    static protected $map = [
        'type' => true,
        'id' => true,
        'chatId' => true,
        'mention' => true,
        'metadata' => true,
        'from' => true,
        'readReceiptRequested' => true,
        'timestamp' => true,
        'participants' => true,
        'chatType' => true,
        'url' => true,
        'title' => true,
        'text' => true,
        'noForward' => true,
        'kikJsData' => true,
        'attribution' => Attribution::class,
    ];

    /**
     * The http or https link of the URL of the website.
     *
     * @var string
     */
    protected $url;

    /**
     * A title to be displayed at the top of the message.
     *
     * @var string
     */
    protected $title;

    /**
     * Some text to be displayed in the middle of the message.
     *
     * @var string
     */
    protected $text;

    /**
     * If true, the message will not be able to be forwarded to other recipients.
     *
     * @var bool
     */
    protected $noForward;

    /**
     * A JSON payload that would be passed to a website using Kik.js.
     *
     * @var array
     */
    protected $kikJsData;

    /**
     * An Attribution object.
     *
     * @var Attribution
     */
    protected $attribution;


    /**
     * TextMessage constructor.
     */
    public function __construct()
    {
        $this->type = 'link';
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return bool
     */
    public function isNoForward()
    {
        return $this->noForward;
    }

    /**
     * @param bool $noForward
     */
    public function setNoForward($noForward)
    {
        $this->noForward = (bool)$noForward;
    }

    /**
     * @return array
     */
    public function getKikJsData()
    {
        return $this->kikJsData;
    }

    /**
     * @param array $kikJsData
     */
    public function setKikJsData(array $kikJsData)
    {
        $this->kikJsData = $kikJsData;
    }

    /**
     * @return Attribution
     */
    public function getAttribution()
    {
        return $this->attribution;
    }

    /**
     * @param Attribution $attribution
     */
    public function setAttribution($attribution)
    {
        $this->attribution = $attribution;
    }
}
