<?php

namespace Advisay\Kik\Api\Types\Message\Received;

use Advisay\Kik\Api\Types\Message\Attribution;

/**
 * Class PictureMessage
 * These apply to all Picture messages that are sent to your bot.
 *
 * @package Advisay\Kik\Api\Types\Message\Received
 */
class PictureMessage extends Message
{
    /**
     * {@inheritdoc}
     *
     * @var array
     */
    static protected $requiredParams = [
        'type',
        'id',
        'chatId',
        'mention',
        'from',
        'readReceiptRequested',
        'timestamp',
        'participants',
        'picUrl',
    ];

    /**
     * {@inheritdoc}
     *
     * @var array
     */
    static protected $map = [
        'type' => true,
        'id' => true,
        'chatId' => true,
        'mention' => true,
        'metadata' => true,
        'from' => true,
        'readReceiptRequested' => true,
        'timestamp' => true,
        'participants' => true,
        'chatType' => true,
        'picUrl' => true,
        'attribution' => Attribution::class,
    ];

    /**
     * The URL of the full-size picture.
     * The image should be JPEG-encoded and under 1MB in size. Received picture links expire after 30 days.
     *
     * @var string
     */
    protected $picUrl;

    /**
     * An Attribution object. If set to "gallery" instead, the message will appear with the default gallery message name
     * and icon. If set to "camera" the message will appear with the default camera message name and icon.
     *
     * @var Attribution
     */
    protected $attribution;


    /**
     * PictureMessage constructor.
     */
    public function __construct()
    {
        $this->type = 'picture';
    }

    /**
     * @return string
     */
    public function getPicUrl()
    {
        return $this->picUrl;
    }

    /**
     * @param string $picUrl
     */
    public function setPicUrl($picUrl)
    {
        $this->picUrl = $picUrl;
    }

    /**
     * @return Attribution
     */
    public function getAttribution()
    {
        return $this->attribution;
    }

    /**
     * @param Attribution $attribution
     */
    public function setAttribution($attribution)
    {
        $this->attribution = $attribution;
    }
}
