<?php

namespace Advisay\Kik\Api\Types\Message\Received;

/**
 * Class TextMessage
 * These apply to all Text messages that are sent to your bot.
 *
 * @package Advisay\Kik\Api\Types\Message\Received
 */
class TextMessage extends Message
{
    /**
     * {@inheritdoc}
     *
     * @var array
     */
    static protected $requiredParams = [
        'type',
        'id',
        'chatId',
        'mention',
        'from',
        'readReceiptRequested',
        'timestamp',
        'participants',
        'body',
    ];

    /**
     * {@inheritdoc}
     *
     * @var array
     */
    static protected $map = [
        'type' => true,
        'id' => true,
        'chatId' => true,
        'mention' => true,
        'metadata' => true,
        'from' => true,
        'readReceiptRequested' => true,
        'timestamp' => true,
        'participants' => true,
        'chatType' => true,
        'body' => true,
    ];

    /**
     * The text of the message.
     *
     * @var string
     */
    protected $body;


    /**
     * TextMessage constructor.
     */
    public function __construct()
    {
        $this->type = 'text';
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param string $body
     */
    public function setBody($body)
    {
        $this->body = $body;
    }
}
