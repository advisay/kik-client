<?php

namespace Advisay\Kik\Api\Types\Message\Received;

use Advisay\Kik\Api\Types\Message\BaseMessage;

/**
 * Class Message
 * These apply to all messages that are sent to your bot.
 *
 * @package Advisay\Kik\Api\Types\Message\Received
 */
abstract class Message extends BaseMessage
{
    /**
     * Received message types
     */
    const TYPE_TEXT = "text";
    const TYPE_LINK = "link";
    const TYPE_PICTURE = "picture";
    const TYPE_SCAN_DATA = "scan-data";
    const TYPE_START_CHATING = "start-chatting";

    /**
     * {@inheritdoc}
     *
     * @var array
     */
    static protected $requiredParams = [
        'type',
        'id',
        'chatId',
        'mention',
        'metadata',
        'from',
        'readReceiptRequested',
        'timestamp',
        'participants',
    ];

    /**
     * {@inheritdoc}
     *
     * @var array
     */
    static protected $map = [
        'type' => true,
        'id' => true,
        'chatId' => true,
        'mention' => true,
        'metadata' => true,
        'from' => true,
        'readReceiptRequested' => true,
        'timestamp' => true,
        'participants' => true,
        'chatType' => true,
    ];

    /**
     * The Kik username of the sender of the message.
     *
     * @var string
     */
    protected $from;

    /**
     * If true, a read receipt was requested for the message. You will not normally see these set on messages unless
     * you've set manuallySendReadReceipts to true in your bot configuration. Well-behaving API users should send a
     * message of type read-receipt with messageIds containing the ID specified in this message's id attribute.
     *
     * @var string
     */
    protected $readReceiptRequested;

    /**
     * The epoch timestamp (in milliseconds) indicating when the message was sent.
     *
     * @var int
     */
    protected $timestamp;

    /**
     * The list of participants in the chat.
     *
     * @var array
     */
    protected $participants;

    /**
     * The type of conversation the message originated from. This field has three possible values: direct, private,
     * and public. If it is set to direct, the user messaged in a one on one conversation with your bot. If the value
     * is set to private, then the user mentioned the bot from within a private chat/group with another user (or users).
     * If the value is set to public, then the user mentioned the bot from within a public group..
     *
     * These apply to all chat messages - excluding read receipts and delivery receipts
     *
     * @var string
     */
    protected $chatType;


    /**
     * @return string
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param string $from
     */
    public function setFrom($from)
    {
        $this->from = $from;
    }

    /**
     * @return string
     */
    public function getReadReceiptRequested()
    {
        return $this->readReceiptRequested;
    }

    /**
     * @param string $readReceiptRequested
     */
    public function setReadReceiptRequested($readReceiptRequested)
    {
        $this->readReceiptRequested = $readReceiptRequested;
    }

    /**
     * @return int
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @param int $timestamp
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    }

    /**
     * @return array
     */
    public function getParticipants()
    {
        return $this->participants;
    }

    /**
     * @param array $participants
     */
    public function setParticipants(array $participants)
    {
        $this->participants = $participants;
    }

    /**
     * @return string
     */
    public function getChatType()
    {
        return $this->chatType;
    }

    /**
     * @param string $chatType
     */
    public function setChatType($chatType)
    {
        $this->chatType = $chatType;
    }
}
