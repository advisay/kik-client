<?php

namespace Advisay\Kik\Api\Types\Message\Received;

/**
 * Class ScanDataMessage
 * When a user scans this Kik Code, the scan data that was associated with that Kik Code will be relayed back to your
 * bot by the user's client. This scan data can be used to identify which specific Kik Code was scanned and by which user.
 *
 * @package Advisay\Kik\Api\Types\Message\Received
 */
class ScanDataMessage extends Message
{
    /**
     * {@inheritdoc}
     *
     * @var array
     */
    static protected $requiredParams = [
        'type',
        'id',
        'chatId',
        'mention',
        'from',
        'readReceiptRequested',
        'timestamp',
        'participants',
        'data',
    ];

    /**
     * {@inheritdoc}
     *
     * @var array
     */
    static protected $map = [
        'type' => true,
        'id' => true,
        'chatId' => true,
        'mention' => true,
        'metadata' => true,
        'from' => true,
        'readReceiptRequested' => true,
        'timestamp' => true,
        'participants' => true,
        'chatType' => true,
        'data' => true,
    ];

    /**
     * The exact string data that was embedded in the Kik Code that the sender just scanned.
     *
     * @var string
     */
    protected $data;


    /**
     * ScanDataMessage constructor.
     */
    public function __construct()
    {
        $this->type = 'scan-data';
    }

    /**
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param string $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }
}
