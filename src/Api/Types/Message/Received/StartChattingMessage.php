<?php

namespace Advisay\Kik\Api\Types\Message\Received;

/**
 * Class StartChattingMessage
 * This message is sent to your bot when a new user starts chatting with your bot. This allows you to send a welcome message.
 *
 * @package Advisay\Kik\Api\Types\Message\Received
 */
class StartChattingMessage extends Message
{
    /**
     * {@inheritdoc}
     *
     * @var array
     */
    static protected $requiredParams = [
        'type',
        'id',
        'from',
        'timestamp',
    ];

    /**
     * StartChattingMessage constructor.
     */
    public function __construct()
    {
        $this->type = 'start-chatting';
    }
}
