<?php

namespace Advisay\Kik\Api\Types\Message;

use Advisay\Kik\Api\BaseType;
use Advisay\Kik\Api\TypeInterface;

/**
 * Class Attribution
 * Attribution objects describe the information that appears in the bar at the bottom of video, picture and link messages.
 *
 * @package Advisay\Kik\Api\Types\BaseMessage
 */
class Attribution extends BaseType implements TypeInterface
{
    /**
     * {@inheritdoc}
     *
     * @var array
     */
    static protected $requiredParams = ['name'];

    /**
     * {@inheritdoc}
     *
     * @var array
     */
    static protected $map = [
        'name' => true,
        'iconUrl' => true
    ];

    /**
     * The name that will appear in the attribution bar.
     *
     * @var string
     */
    protected $name;

    /**
     * The URL specifying an icon that will appear in the attribution bar.
     *
     * @var string
     */
    protected $iconUrl;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getIconUrl()
    {
        return $this->iconUrl;
    }

    /**
     * @param string $iconUrl
     */
    public function setIconUrl($iconUrl)
    {
        $this->iconUrl = $iconUrl;
    }
}
