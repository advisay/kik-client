<?php

namespace Advisay\Kik\Api\Types\Message\Sent;

use Advisay\Kik\Api\Types\Keyboard\ArrayOfKeyboards;
use Advisay\Kik\Api\Types\Message\Attribution;

/**
 * Class PictureMessage
 * These apply to all Picture messages that your bot sends.
 *
 * @package Advisay\Kik\Api\Types\Message\Sent
 */
class PictureMessage extends Message
{
    /**
     * {@inheritdoc}
     *
     * @var array
     */
    static protected $requiredParams = [
        'type',
        'to',
        'picUrl',
    ];

    /**
     * {@inheritdoc}
     *
     * @var array
     */
    static protected $map = [
        'type' => true,
        'id' => true,
        'chatId' => true,
        'mention' => true,
        'metadata' => true,
        'to' => true,
        'delay' => true,
        'picUrl' => true,
        'attribution' => Attribution::class,
        'keyboards' => ArrayOfKeyboards::class,
    ];

    /**
     * The URL of the full-size picture.
     * The image should be JPEG-encoded and under 1MB in size. Received picture links expire after 30 days.
     *
     * @var string
     */
    protected $picUrl;

    /**
     * An Attribution object. If set to "gallery" instead, the message will appear with the default gallery message name
     * and icon. If set to "camera" the message will appear with the default camera message name and icon.
     *
     * @var Attribution
     */
    protected $attribution;

    /**
     * A list of keyboard objects that will be sent to the users in this chat.
     *
     * @var array
     */
    protected $keyboards;


    /**
     * PictureMessage constructor.
     * @param string|null $to
     * @param string|null $picUrl
     */
    public function __construct(string $to = null, string $picUrl = null)
    {
        $this->type = 'picture';
        $this->to = $to;
        $this->picUrl = $picUrl;
    }

    /**
     * @return string
     */
    public function getPicUrl()
    {
        return $this->picUrl;
    }

    /**
     * @param string $picUrl
     */
    public function setPicUrl($picUrl)
    {
        $this->picUrl = $picUrl;
    }

    /**
     * @return Attribution
     */
    public function getAttribution()
    {
        return $this->attribution;
    }

    /**
     * @param Attribution $attribution
     */
    public function setAttribution($attribution)
    {
        $this->attribution = $attribution;
    }

    /**
     * @return array|null
     */
    public function getKeyboards()
    {
        return $this->keyboards;
    }

    /**
     * @param array $keyboards
     */
    public function setKeyboards(array $keyboards)
    {
        $this->keyboards = $keyboards;
    }
}
