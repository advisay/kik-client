<?php

namespace Advisay\Kik\Api\Types\Message\Sent;

use Advisay\Kik\Api\Types\Message\BaseMessage;

/**
 * Class Message
 * These apply to all messages that your bot sends.
 *
 * @package Advisay\Kik\Api\Types\Message\Sent
 */
abstract class Message extends BaseMessage
{
    /**
     * {@inheritdoc}
     *
     * @var array
     */
    static protected $requiredParams = ['type', 'id', 'chatId', 'mention', 'metadata', 'to', 'delay'];

    /**
     * {@inheritdoc}
     *
     * @var array
     */
    static protected $map = [
        'type' => true,
        'id' => true,
        'chatId' => true,
        'mention' => true,
        'metadata' => true,
        'to' => true,
        'delay' => true,
    ];

    /**
     * The Kik username of the recipient of the message.
     *
     * @var string
     */
    protected $to;

    /**
     * An interval (in milliseconds) to wait before sending the message.
     *
     * @var int
     */
    protected $delay = 0;

    /**
     * @return string
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param string $to
     */
    public function setTo($to)
    {
        $this->to = $to;
    }

    /**
     * @return int
     */
    public function getDelay()
    {
        return $this->delay;
    }

    /**
     * @param int $delay
     */
    public function setDelay($delay)
    {
        $this->delay = $delay;
    }
}
