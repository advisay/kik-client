<?php

namespace Advisay\Kik\Api\Types\Message\Sent;

use Advisay\Kik\Api\Types\Keyboard\ArrayOfKeyboards;

/**
 * Class TextMessage
 * These apply to all Text messages that your bot sends.
 *
 * @package Advisay\Kik\Api\Types\Message\Sent
 */
class TextMessage extends Message
{
    /**
     * {@inheritdoc}
     *
     * @var array
     */
    static protected $requiredParams = [
        'type',
        'chatId',
        'to',
        'body',
        'keyboards',
    ];

    /**
     * {@inheritdoc}
     *
     * @var array
     */
    static protected $map = [
        'type' => true,
        'id' => true,
        'chatId' => true,
        'mention' => true,
        'metadata' => true,
        'to' => true,
        'delay' => true,
        'body' => true,
        'typeTime' => true,
        'keyboards' => ArrayOfKeyboards::class,
    ];

    /**
     * The text of the message.
     *
     * @var string
     */
    protected $body;

    /**
     * An interval (in milliseconds) to appear to be typing to the recipient before the message is sent.
     * This occurs after delay.
     *
     * @var int
     */
    protected $typeTime = 0;

    /**
     * A list of keyboard objects that will be sent to the users in this chat.
     *
     * @var array
     */
    protected $keyboards;


    /**
     * TextMessage constructor.
     * @param string $to
     * @param string $body
     */
    public function __construct(string $to = null, string $body = null)
    {
        $this->type = 'text';
        $this->to = $to;
        $this->body = $body;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param string $body
     */
    public function setBody($body)
    {
        $this->body = $body;
    }

    /**
     * @return int
     */
    public function getTypeTime()
    {
        return $this->typeTime;
    }

    /**
     * @param int $typeTime
     */
    public function setTypeTime($typeTime)
    {
        $this->typeTime = $typeTime;
    }

    /**
     * @return array|null
     */
    public function getKeyboards()
    {
        return $this->keyboards;
    }

    /**
     * @param array $keyboards
     */
    public function setKeyboards(array $keyboards)
    {
        $this->keyboards = $keyboards;
    }
}
