<?php

namespace Advisay\Kik\Api\Types\Message;

use Advisay\Kik\Api\BaseType;
use Advisay\Kik\Api\TypeInterface;

/**
 * Class BaseMessage
 * The Kik Bot API uses a number of different message types to distinguish the types of interactions between users and bots.
 *
 * @package Advisay\Kik\Api\Types\Message
 */
abstract class BaseMessage extends BaseType implements TypeInterface
{
    /**
     * {@inheritdoc}
     *
     * @var array
     */
    static protected $requiredParams = ['type', 'id', 'chatId', 'mention', 'metadata'];

    /**
     * {@inheritdoc}
     *
     * @var array
     */
    static protected $map = [
        'type' => true,
        'id' => true,
        'chatId' => true,
        'mention' => true,
        'metadata' => true,
    ];

    /**
     * The type of message.
     *
     * @var string
     */
    protected $type;

    /**
     * ID for this message. Use this to link messages to receipts. This will always be present for received messages.
     *
     * @var string
     */
    protected $id;

    /**
     * The identifier for the conversation your bot is involved in. This field is recommended for all responses in
     * order for messages to be routed correctly (for example, if you're messaging a user in a group).
     *
     * @var string
     */
    protected $chatId;

    /**
     * The username of the bot mentioned in the message.
     *
     * @var string
     */
    protected $mention;

    /**
     * Metadata that was provided by your bot when sending the user a suggested response.
     *
     * @var string
     */
    protected $metadata;

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getChatId()
    {
        return $this->chatId;
    }

    /**
     * @param string $chatId
     */
    public function setChatId($chatId)
    {
        $this->chatId = $chatId;
    }

    /**
     * @return string
     */
    public function getMention()
    {
        return $this->mention;
    }

    /**
     * @param string $mention
     */
    public function setMention($mention)
    {
        $this->mention = $mention;
    }

    /**
     * @return string
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * @param string $metadata
     */
    public function setMetadata($metadata)
    {
        $this->metadata = $metadata;
    }
}
