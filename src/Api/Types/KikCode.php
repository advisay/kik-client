<?php

namespace Advisay\Kik\Api\Types;

use Advisay\Kik\Api\BaseType;
use Advisay\Kik\Api\TypeInterface;

/**
 * Class KikCode
 * This object represents a Kik code.
 *
 * @package Advisay\Kik\Api\Types
 */
class KikCode extends BaseType implements TypeInterface
{
    /**
     * {@inheritdoc}
     *
     * @var array
     */
    static protected $requiredParams = ['id'];

    /**
     * {@inheritdoc}
     *
     * @var array
     */
    static protected $map = [
        'id' => true
    ];

    /**
     * Hex-encoded 160-bit ID for the Kik Code
     *
     * @var string
     */
    protected $id;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
}
