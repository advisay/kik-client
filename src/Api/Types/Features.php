<?php

namespace Advisay\Kik\Api\Types;

use Advisay\Kik\Api\BaseType;
use Advisay\Kik\Api\TypeInterface;

/**
 * Class Features
 * This object represents features.
 *
 * @package Advisay\Kik\Api\Types
 */
class Features extends BaseType implements TypeInterface
{
    /**
     * {@inheritdoc}
     *
     * @var array
     */
    static protected $requiredParams = [
        'manuallySendReadReceipts',
        'receiveReadReceipts',
        'receiveDeliveryReceipts',
        'receiveIsTyping',
    ];

    /**
     * {@inheritdoc}
     *
     * @var array
     */
    static protected $map = [
        'manuallySendReadReceipts' => true,
        'receiveReadReceipts' => true,
        'receiveDeliveryReceipts' => true,
        'receiveIsTyping' => true,
    ];

    /**
     * If enabled, your bot will be responsible for sending its own read receipts to users when you receive messages.
     *
     * @var bool
     */
    protected $manuallySendReadReceipts = false;

    /**
     * If enabled, your bot will receive messages of type read-receipt messages from users.
     *
     * @var bool
     */
    protected $receiveReadReceipts = false;

    /**
     * If enabled, your bot will receive messages of type delivery-receipt messages from users.
     *
     * @var bool
     */
    protected $receiveDeliveryReceipts = false;

    /**
     * If enabled, your bot will receive messages of type is-typing messages from users.
     *
     * @var bool
     */
    protected $receiveIsTyping = false;

    /**
     * @return boolean
     */
    public function isManuallySendReadReceipts()
    {
        return $this->manuallySendReadReceipts;
    }

    /**
     * @param boolean $manuallySendReadReceipts
     */
    public function setManuallySendReadReceipts($manuallySendReadReceipts)
    {
        $this->manuallySendReadReceipts = (bool)$manuallySendReadReceipts;
    }

    /**
     * @return boolean
     */
    public function isReceiveReadReceipts()
    {
        return $this->receiveReadReceipts;
    }

    /**
     * @param boolean $receiveReadReceipts
     */
    public function setReceiveReadReceipts($receiveReadReceipts)
    {
        $this->receiveReadReceipts = (bool)$receiveReadReceipts;
    }

    /**
     * @return boolean
     */
    public function isReceiveDeliveryReceipts()
    {
        return $this->receiveDeliveryReceipts;
    }

    /**
     * @param boolean $receiveDeliveryReceipts
     */
    public function setReceiveDeliveryReceipts($receiveDeliveryReceipts)
    {
        $this->receiveDeliveryReceipts = (bool)$receiveDeliveryReceipts;
    }

    /**
     * @return boolean
     */
    public function isReceiveIsTyping()
    {
        return $this->receiveIsTyping;
    }

    /**
     * @param boolean $receiveIsTyping
     */
    public function setReceiveIsTyping($receiveIsTyping)
    {
        $this->receiveIsTyping = (bool)$receiveIsTyping;
    }

}
