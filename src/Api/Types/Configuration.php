<?php

namespace Advisay\Kik\Api\Types;

use Advisay\Kik\Api\BaseType;
use Advisay\Kik\Api\TypeInterface;
use Advisay\Kik\Api\Types\Keyboard\Keyboard;

/**
 * Class Configuration
 * This object represents a Kik Configuration.
 *
 * @package Advisay\Kik\Api\Types
 */
class Configuration extends BaseType implements TypeInterface
{
    /**
     * {@inheritdoc}
     *
     * @var array
     */
    static protected $requiredParams = ['webhook', 'features'];

    /**
     * {@inheritdoc}
     *
     * @var array
     */
    static protected $map = [
        'webhook' => true,
        'features' => Features::class,
        'staticKeyboard' => Keyboard::class,
    ];

    /**
     * A URL to a webhook to which calls will be made when users interact with your bot.
     *
     * @var string
     */
    protected $webhook;

    /**
     * An object describing the features that are active or not active for your bot.
     *
     * @var Features
     */
    protected $features;

    /**
     * Optional. A keyboard object that shows when a user starts to mention your bot in a conversation.
     *
     * @var Keyboard
     */
    protected $staticKeyboard;

    /**
     * @return string
     */
    public function getWebhook()
    {
        return $this->webhook;
    }

    /**
     * @param string $webhook
     */
    public function setWebhook($webhook)
    {
        $this->webhook = $webhook;
    }

    /**
     * @return Features
     */
    public function getFeatures()
    {
        return $this->features;
    }

    /**
     * @param Features $features
     */
    public function setFeatures($features)
    {
        $this->features = $features;
    }

    /**
     * @return Keyboard|null
     */
    public function getStaticKeyboard()
    {
        return $this->staticKeyboard;
    }

    /**
     * @param Keyboard|null $staticKeyboard
     */
    public function setStaticKeyboard($staticKeyboard)
    {
        $this->staticKeyboard = $staticKeyboard;
    }
}
