<?php

namespace Advisay\Kik\Api\Types;

use Advisay\Kik\Api\BaseType;
use Advisay\Kik\Api\TypeInterface;

/**
 * Class UserProfile
 * This object represents a Kik user profile.
 *
 * @package Advisay\Kik\Api\Types
 */
class UserProfile extends BaseType implements TypeInterface
{
    /**
     * {@inheritdoc}
     *
     * @var array
     */
    static protected $requiredParams = ['firstName', 'lastName'];

    /**
     * {@inheritdoc}
     *
     * @var array
     */
    static protected $map = [
        'firstName' => true,
        'lastName' => true,
        'profilePicUrl' => true,
        'profilePicLastModified' => true,
        'timezone' => true,
    ];

    /**
     * The user's first name.
     *
     * @var string
     */
    protected $firstName;

    /**
     * The user's last name.
     *
     * @var string
     */
    protected $lastName;

    /**
     * A URL pointing to the profile image of the user. Will be null if the user has not set a profile picture.
     *
     * @var string
     */
    protected $profilePicUrl;

    /**
     * The epoch timestamp (in milliseconds) indicating when the profile picture was last changed. Will be null if the user has not set a profile picture.
     *
     * @var string
     */
    protected $profilePicLastModified;

    /**
     * The user's IANA timezone name. Will be null if the user's timezone is unknown.
     *
     * @var string
     */
    protected $timezone;

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getProfilePicUrl()
    {
        return $this->profilePicUrl;
    }

    /**
     * @param string $profilePicUrl
     */
    public function setProfilePicUrl($profilePicUrl)
    {
        $this->profilePicUrl = $profilePicUrl;
    }

    /**
     * @return string
     */
    public function getProfilePicLastModified()
    {
        return $this->profilePicLastModified;
    }

    /**
     * @param string $profilePicLastModified
     */
    public function setProfilePicLastModified($profilePicLastModified)
    {
        $this->profilePicLastModified = $profilePicLastModified;
    }

    /**
     * @return string
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * @param string $timezone
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;
    }
}
