# Kik API bundle

A bundle to work with KIK messenger API

## Install

Via Composer

``` bash
$ composer require advisay/kik
```

## Usage

TODO: add usage section

``` php
TODO: add usage php code
```

## Testing

``` bash
$ phpunit
```

## Credits

- [Alexander Yurchik](https://github.com/alexdrupal)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
